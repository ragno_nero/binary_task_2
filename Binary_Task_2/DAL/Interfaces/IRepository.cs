﻿using System;
using System.Collections.Generic;

namespace Binary_Task_2.DAL.Interfaces
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> GetAll();
        void Create(T item);
        T Read(int id);
        void Update(int id, T item);
        void Delete(int id);
    }
}
