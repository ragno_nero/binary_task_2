﻿using Binary_Task_2.Models;

namespace Binary_Task_2.DAL.Interfaces
{
    interface IUnitOfWork
    {
        IRepository<Project> Project { get; }
        IRepository<Task> Task { get; }
        IRepository<TaskState> TaskState { get; }
        IRepository<Team> Team { get; }
        IRepository<User> User { get; }
    }
}
