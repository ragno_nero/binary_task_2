﻿using Binary_Task_2.Models;
using Binary_Task_2.DAL.Interfaces;
using System.Collections.Generic;
using Binary_Task_2.Services;
using System.Linq;
using Binary_Task_2.ExtraModels;

namespace Binary_Task_2.DAL.Repositories
{
    public class ProjectRepository : IRepository<Project>
    {
        private readonly DatabaseService db;

        public ProjectRepository(DatabaseService context)
        {
            db = context;
        }

        public void Create(Project item)
        {
            Project p = db.Projects.LastOrDefault();
            if (p != null)
                item.Id = p.Id + 1;
            else
                item.Id = 1;
            db.Projects.Add(item);
        }

        public Project Read(int id)
        {
            return db.Projects.Where(p => p.Id == id).FirstOrDefault();
        }

        public void Update(int id, Project item)
        {
            int index = db.Projects.IndexOf(db.Projects.Where(p => p.Id == id).FirstOrDefault());
            if (index != -1)
                db.Projects[index] = item;
        }

        public void Delete(int id)
        {
            Project p = Read(id);
            if (p != null)
                db.Projects.Remove(p);
        }

        public IEnumerable<Project> GetAll()
        {
            return db.Projects;
        }

        public Dictionary<string, int> GetUserTaskCount(int performerId)
        {
            return db.Projects.ToDictionary(p => p.Name, p => db.Tasks.Where(t => t.PerformerId == performerId && t.ProjectId == p.Id).Count());
        }

        public DetailedProject GetDetailedProject(int id)
        {
            Project project = db.Projects.Where(p => p.Id == id).FirstOrDefault();
            return db.Users.Join(db.Tasks, u => u.Id, t => t.PerformerId,
                (us, ts) => new { us, ts }).Select(y => new DetailedProject
                {
                    Project = project,
                    LongestTask = db.Tasks.Where(t => t.ProjectId == project.Id && t.Description.Length == db.Tasks.Where(tt => tt.ProjectId == project.Id).Max(m => m.Description.Length)).FirstOrDefault(),
                    ShortestTask = db.Tasks.Where(t => t.ProjectId == project.Id && t.Name.Length == db.Tasks.Where(tt => tt.ProjectId == project.Id).Min(m => m.Name.Length)).FirstOrDefault(),
                    UserCount = db.Projects.Where(p => p.Description.Length > 25 || db.Tasks.Where(t => t.ProjectId == p.Id).Count() > 3).GroupJoin(db.Tasks, p => p.Id, t => t.ProjectId,
                    (p, t) => t.Where(dt => p.Id == dt.ProjectId).Select(s => s.PerformerId)
                    ).ToList().Distinct().Count()
                }
                ).FirstOrDefault();
        }
    }
}
