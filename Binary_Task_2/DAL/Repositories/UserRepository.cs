﻿using Binary_Task_2.Models;
using Binary_Task_2.DAL.Interfaces;
using System.Collections.Generic;
using Binary_Task_2.Services;
using System.Linq;
using Binary_Task_2.ExtraModels;

namespace Binary_Task_2.DAL.Repositories
{
    public class UserRepository : IRepository<User>
    {
        private readonly DatabaseService db;

        public UserRepository(DatabaseService context)
        {
            db = context;
        }

        public void Create(User item)
        {
            User u = db.Users.LastOrDefault();
            if (u != null)
                item.Id = u.Id + 1;
            else
                item.Id = 1;
            db.Users.Add(item);
        }

        public User Read(int id)
        {
            return db.Users.Where(u => u.Id == id).FirstOrDefault();
        }

        public void Update(int id, User item)
        {
            int index = db.Users.IndexOf(db.Users.Where(t => t.Id == id).FirstOrDefault());
            if (index != -1)
                db.Users[index] = item;
        }

        public void Delete(int id)
        {
            User u = Read(id);
            if (u != null)
                db.Users.Remove(u);
        }

        public IEnumerable<User> GetAll()
        {
            return db.Users;
        }

        public List<UserWithTasks> GetUsersWithTasks()
        {
            return db.Users.GroupJoin(db.Tasks, u => u.Id, t => t.PerformerId,
                (us, ts) => new UserWithTasks
                {
                    User = us,
                    Tasks = ts.Where(t => t.PerformerId == us.Id).OrderByDescending(t => t.Name.Length).ToList()
                }
                ).Where(u => u.Tasks.Count != 0).OrderBy(u => u.User.FirstName).ToList();
        }

        public DetailedUser GetDetailedUser(int id)
        {
            User user = db.Users.Where(u => u.Id == id).FirstOrDefault();
            return db.Tasks.Join(db.Projects, t => t.ProjectId, p => p.Id, (pr, t) => new { pr, t }).Select(y => new DetailedUser
            {
                User = user,
                LastProject = db.Projects.Select(p => p).Where(t => t.CreatedAt == db.Projects.Max(d => d.CreatedAt)).SingleOrDefault(),
                TaskCount = db.Tasks.Where(t => t.ProjectId == db.Projects.Select(p => p).Where(p => p.CreatedAt == db.Projects.Max(d => d.CreatedAt)).SingleOrDefault()?.Id).Count(),
                NotFinishedTaskCount = db.Tasks.Where(t => t.State != States.Finished && t.PerformerId == user.Id).Count(),
                LongestTask = db.Tasks.Where(t => t.FinishedAt - t.CreatedAt == db.Tasks.Max(w => w.FinishedAt - w.CreatedAt)).SingleOrDefault()
            }).FirstOrDefault();
        }
    }
}
