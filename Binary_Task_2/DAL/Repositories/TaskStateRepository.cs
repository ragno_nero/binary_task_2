﻿using Binary_Task_2.Models;
using Binary_Task_2.DAL.Interfaces;
using System.Collections.Generic;
using Binary_Task_2.Services;
using System.Linq;

namespace Binary_Task_2.DAL.Repositories
{
    public class TaskStateRepository : IRepository<TaskState>
    {
        private readonly DatabaseService db;

        public TaskStateRepository(DatabaseService context)
        {
            db = context;
        }

        public void Create(TaskState item)
        {
            TaskState t = db.TaskStates.LastOrDefault();
            if (t != null)
                item.Id = t.Id + 1;
            else
                item.Id = 1;
            db.TaskStates.Add(item);
        }

        public TaskState Read(int id)
        {
            return db.TaskStates.Where(t => t.Id == id).FirstOrDefault();
        }

        public void Update(int id, TaskState item)
        {
            int index = db.TaskStates.IndexOf(db.TaskStates.Where(t => t.Id == id).FirstOrDefault());
            if (index != -1)
                db.TaskStates[index] = item;
        }

        public void Delete(int id)
        {
            TaskState t = Read(id);
            if (t != null)
                db.TaskStates.Remove(t);
        }

        public IEnumerable<TaskState> GetAll()
        {
            return db.TaskStates;
        }
    }
}
