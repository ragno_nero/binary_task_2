﻿using Binary_Task_2.Models;
using Binary_Task_2.DAL.Interfaces;
using System.Collections.Generic;
using Binary_Task_2.Services;
using System.Linq;
using System;
using Binary_Task_2.ExtraModels;

namespace Binary_Task_2.DAL.Repositories
{
    public class TeamRepository : IRepository<Team>
    {
        private readonly DatabaseService db;

        public TeamRepository(DatabaseService context)
        {
            db = context;
        }

        public void Create(Team item)
        {
            Team t = db.Teams.LastOrDefault();
            if (t != null)
                item.Id = t.Id + 1;
            else
                item.Id = 1;
            db.Teams.Add(item);
        }

        public Team Read(int id)
        {
            return db.Teams.Where(t => t.Id == id).FirstOrDefault();
        }

        public void Update(int id, Team item)
        {
            int index = db.Teams.IndexOf(db.Teams.Where(t => t.Id == id).FirstOrDefault());
            if (index != -1)
                db.Teams[index] = item;
        }

        public void Delete(int id)
        {
            Team t = Read(id);
            if (t != null)
                db.Teams.Remove(t);
        }

        public IEnumerable<Team> GetAll()
        {
            return db.Teams;
        }

        public List<DetailedTeam> GetDetailedTeam()
        {
            return db.Teams.GroupJoin(db.Users, t => t.Id, u => u.TeamId,
                (tm, us) => new DetailedTeam
                {
                    Id = tm.Id,
                    Name = tm.Name,
                    Users = us.Where(u => u.Birthday.AddYears(12) <= DateTime.Now && tm.Id == u.TeamId).OrderByDescending(p => p.RegisteredAt).ToList()
                }
                ).Where(u => u.Users.Count != 0).ToList();
        }
    }
}
