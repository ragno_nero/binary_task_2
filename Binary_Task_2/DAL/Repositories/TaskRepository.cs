﻿using Binary_Task_2.Models;
using Binary_Task_2.DAL.Interfaces;
using System.Collections.Generic;
using Binary_Task_2.Services;
using System.Linq;
using System;

namespace Binary_Task_2.DAL.Repositories
{
    public class TaskRepository : IRepository<Task>
    {
        private readonly DatabaseService db;

        public TaskRepository(DatabaseService context)
        {
            db = context;
        }

        public void Create(Task item)
        {
            Task t = db.Tasks.LastOrDefault();
            if (t != null)
                item.Id = t.Id + 1;
            else
                item.Id = 1;
            db.Tasks.Add(item);
        }

        public Task Read(int id)
        {
            return db.Tasks.Where(t => t.Id == id).FirstOrDefault();
        }

        public void Update(int id, Task item)
        {
            int index = db.Tasks.IndexOf(db.Tasks.Where(t => t.Id == id).FirstOrDefault());
            if (index != -1)
                db.Tasks[index] = item;
        }

        public void Delete(int id)
        {
            Task t = Read(id);
            if (t != null)
                db.Tasks.Remove(t);
        }

        public IEnumerable<Task> GetAll()
        {
            return db.Tasks;
        }

        public List<Task> GetTaskListByUser(int performerId)
        {
            return db.Tasks.Where(t => t.PerformerId == performerId && t.Name.Length < 45).ToList();
        }

        public List<Tuple<int, string>> GetFinishedTasks(int performerId)
        {
            return db.Tasks.Where(t => t.State == States.Finished && t.PerformerId == performerId && t.FinishedAt.Year == 2019).Select(t => Tuple.Create(t.Id, t.Name)).ToList();
        }
    }
}
