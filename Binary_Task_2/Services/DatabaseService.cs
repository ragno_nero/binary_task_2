﻿using Binary_Task_2.Models;
using System.Collections.Generic;

namespace Binary_Task_2.Services
{
    public class DatabaseService
    {
        public readonly List<Project> Projects;
        public readonly List<Task> Tasks;
        public readonly List<TaskState> TaskStates;
        public readonly List<Team> Teams;
        public readonly List<User> Users;

        public DatabaseService()
        {
            Projects = new List<Project>();
            Tasks = new List<Task>();
            TaskStates = new List<TaskState>();
            Teams = new List<Team>();
            Users = new List<User>();
        }
    }
}
