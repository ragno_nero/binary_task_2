﻿using System.Collections.Generic;
using System.Linq;
using Binary_Task_2.DAL;
using Binary_Task_2.Models;
using Microsoft.AspNetCore.Mvc;

namespace Binary_Task_2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly DALInstance _dal;

        public UserController(DALInstance serv)
        {
            _dal = serv;
        }
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<User>> Get()
        {
            return _dal.User.GetAll().ToList();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<User> Get(int id)
        {
            return _dal.User.Read(id);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] User value)
        {
            _dal.User.Create(value);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] User value)
        {
            _dal.User.Update(id, value);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _dal.User.Delete(id);
        }
    }
}