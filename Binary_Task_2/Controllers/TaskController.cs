﻿using System.Collections.Generic;
using System.Linq;
using Binary_Task_2.DAL;
using Binary_Task_2.Models;
using Microsoft.AspNetCore.Mvc;

namespace Binary_Task_2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly DALInstance _dal;

        public TaskController(DALInstance serv)
        {
            _dal = serv;
        }
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<Task>> Get()
        {
            return _dal.Task.GetAll().ToList();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<Task> Get(int id)
        {
            return _dal.Task.Read(id);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] Task value)
        {
            _dal.Task.Create(value);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Task value)
        {
            _dal.Task.Update(id, value);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _dal.Task.Delete(id);
        }
    }
}