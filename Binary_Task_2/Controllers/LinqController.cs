﻿using System;
using System.Collections.Generic;
using Binary_Task_2.DAL;
using Binary_Task_2.DAL.Repositories;
using Binary_Task_2.ExtraModels;
using Binary_Task_2.Models;
using Microsoft.AspNetCore.Mvc;

namespace Binary_Task_2.Controllers
{
    [ApiController]
    public class LinqController : ControllerBase
    {
        private readonly DALInstance _dal;

        public LinqController(DALInstance serv)
        {
            _dal = serv;
        }

        [HttpGet]
        [Route("api/[controller]/taskcount/{id}")]
        public ActionResult<Dictionary<string,int>> GetUserTaskCount(int id)
        {
            return ((ProjectRepository)_dal.Project).GetUserTaskCount(id);
        }

        [HttpGet]
        [Route("api/[controller]/usertask/{id}")]
        public ActionResult<List<Task>> GetTaskListByUser(int id)
        {
            return ((TaskRepository)_dal.Task).GetTaskListByUser(id);
        }

        [HttpGet]
        [Route("api/[controller]/finishedtasks/{id}")]
        public ActionResult<List<Tuple<int, string>>> GetFinishedTasks(int id)
        {
            return ((TaskRepository)_dal.Task).GetFinishedTasks(id);
        }

        [HttpGet]
        [Route("api/[controller]/detailedteam")]
        public ActionResult<List<DetailedTeam>> GetDetailedTeam()
        {
            return ((TeamRepository)_dal.Team).GetDetailedTeam();
        }

        [HttpGet]
        [Route("api/[controller]/userwithtasks")]
        public ActionResult<List<UserWithTasks>> GetUserWithTasks()
        {
            return ((UserRepository)_dal.User).GetUsersWithTasks();
        }

        [HttpGet]
        [Route("api/[controller]/detaileduser/{id}")]
        public ActionResult<DetailedUser> GetDetailedUser(int id)
        {
            return ((UserRepository)_dal.User).GetDetailedUser(id);
        }

        [HttpGet]
        [Route("api/[controller]/detailedproject/{id}")]
        public ActionResult<DetailedProject> GetDetailedProject(int id)
        {
            return ((ProjectRepository)_dal.Project).GetDetailedProject(id);
        }
    }
}