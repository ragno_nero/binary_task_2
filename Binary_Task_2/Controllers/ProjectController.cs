﻿using System.Collections.Generic;
using System.Linq;
using Binary_Task_2.DAL;
using Binary_Task_2.Models;
using Microsoft.AspNetCore.Mvc;

namespace Binary_Task_2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        private readonly DALInstance _dal;

        public ProjectController(DALInstance serv)
        {
            _dal = serv;
        }
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<Project>> Get()
        {
            return _dal.Project.GetAll().ToList();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<Project> Get(int id)
        {
            return _dal.Project.Read(id);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] Project value)
        {
            _dal.Project.Create(value);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Project value)
        {
            _dal.Project.Update(id, value);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _dal.Project.Delete(id);
        }
    }
}