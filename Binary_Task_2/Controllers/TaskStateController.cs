﻿using System.Collections.Generic;
using System.Linq;
using Binary_Task_2.DAL;
using Binary_Task_2.Models;
using Microsoft.AspNetCore.Mvc;

namespace Binary_Task_2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskStateController : ControllerBase
    {
        private readonly DALInstance _dal;

        public TaskStateController(DALInstance serv)
        {
            _dal = serv;
        }
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<TaskState>> Get()
        {
            return _dal.TaskState.GetAll().ToList();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<TaskState> Get(int id)
        {
            return _dal.TaskState.Read(id);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] TaskState value)
        {
            _dal.TaskState.Create(value);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] TaskState value)
        {
            _dal.TaskState.Update(id, value);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _dal.TaskState.Delete(id);
        }
    }
}