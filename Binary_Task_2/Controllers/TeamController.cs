﻿using System.Collections.Generic;
using System.Linq;
using Binary_Task_2.DAL;
using Binary_Task_2.Models;
using Microsoft.AspNetCore.Mvc;

namespace Binary_Task_2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamController : ControllerBase
    {
        private readonly DALInstance _dal;

        public TeamController(DALInstance serv)
        {
            _dal = serv;
        }
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<Team>> Get()
        {
            return _dal.Team.GetAll().ToList();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<Team> Get(int id)
        {
            return _dal.Team.Read(id);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] Team value)
        {
            _dal.Team.Create(value);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Team value)
        {
            _dal.Team.Update(id, value);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _dal.Team.Delete(id);
        }
    }
}