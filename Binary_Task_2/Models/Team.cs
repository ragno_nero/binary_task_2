﻿using System;
using Newtonsoft.Json;

namespace Binary_Task_2.Models
{
    public class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
