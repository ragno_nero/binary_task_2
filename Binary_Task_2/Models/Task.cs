﻿using System;
using Newtonsoft.Json;

namespace Binary_Task_2.Models
{
    public class Task
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Description { get; set; }
        public DateTime FinishedAt { get; set; }
        public States State { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
    }

    public enum States {
        Created,
        Started,
        Finished,
        Canceled
    }
}
