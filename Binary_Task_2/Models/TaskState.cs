﻿using Newtonsoft.Json;

namespace Binary_Task_2.Models
{
    public class TaskState
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
