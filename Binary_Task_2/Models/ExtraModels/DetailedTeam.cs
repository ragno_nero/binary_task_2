﻿using Binary_Task_2.Models;
using System.Collections.Generic;

namespace Binary_Task_2.ExtraModels
{
    public class DetailedTeam
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<User> Users { get; set; }
    }
}
