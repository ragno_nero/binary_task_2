﻿using Binary_Task_2.Models;

namespace Binary_Task_2.ExtraModels
{
    public class DetailedUser
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
        public int TaskCount { get; set; }
        public int NotFinishedTaskCount { get; set; }
        public Task LongestTask { get; set; }
    }
}
