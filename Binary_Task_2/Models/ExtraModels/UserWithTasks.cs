﻿using Binary_Task_2.Models;
using System.Collections.Generic;

namespace Binary_Task_2.ExtraModels
{
    public class UserWithTasks
    {
        public User User { get; set; }
        public List<Task> Tasks { get; set; }
    }
}
