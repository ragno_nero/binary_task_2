﻿using Binary_Task_2.Models;

namespace Binary_Task_2.ExtraModels
{
    public class DetailedProject
    {
        public Project Project { get; set; }
        public Task LongestTask { get; set; }
        public Task ShortestTask { get; set; }
        public int UserCount { get; set; }
    }
}
